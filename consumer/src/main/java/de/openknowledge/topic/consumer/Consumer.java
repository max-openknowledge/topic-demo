package de.openknowledge.topic.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@MessageDriven
public class Consumer implements MessageListener {

  private static final Logger LOG = LoggerFactory.getLogger(Consumer.class);

  @Override
  public void onMessage(Message message) {
    try {
      String text = ((TextMessage)message).getText();

      LOG.info("Received message: {}", text);

    } catch (JMSException e) {
      LOG.error(e.getMessage(), e);
    }
  }
}
