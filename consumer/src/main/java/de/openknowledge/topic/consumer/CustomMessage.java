package de.openknowledge.topic.consumer;

public class CustomMessage {

  private String msg;

  public CustomMessage() {
  }

  public CustomMessage(String msg) {
    this.msg = msg;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  @Override
  public String toString() {
    return "CustomMessage{" + "msg='" + msg + '\'' + '}';
  }
}
