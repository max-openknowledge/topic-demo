package de.openknowledge.topic.producer;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 */
@ApplicationPath("/data")
public class ProducerRestApplication extends Application {
}
