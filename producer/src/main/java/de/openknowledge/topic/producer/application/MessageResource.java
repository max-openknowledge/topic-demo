package de.openknowledge.topic.producer.application;

import de.openknowledge.topic.producer.domain.Producer;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 *
 */
@Path("/message")
@Singleton
public class MessageResource {

  @Inject
  private Producer messageSender;

  @GET
  public Response trigger(@QueryParam("msg") String msg) {
    messageSender.send(msg);
    return Response
        .ok()
        .build();
  }

}
