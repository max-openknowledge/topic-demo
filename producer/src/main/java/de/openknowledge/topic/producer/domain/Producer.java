package de.openknowledge.topic.producer.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnectionFactory;
import javax.json.bind.JsonbBuilder;

@ApplicationScoped
public class Producer {

  private static final Logger LOG = LoggerFactory.getLogger(Producer.class);

  @Resource(lookup = "JMSTopicFactory")
  private TopicConnectionFactory jmsFactory;

  @Resource(lookup = "JMSTopic")
  private Topic jmsTopic;

  public void send(String msg) {
    try (Connection connection = jmsFactory.createConnection();
        Session session = connection.createSession();
        MessageProducer producer = session.createProducer(jmsTopic)) {

      LOG.debug("Sending to topic: {}", jmsTopic.getTopicName());

      TextMessage textMessage = session.createTextMessage();
      String customMessage = createCustomMessage(msg);

      LOG.info("Sending message: {}", customMessage);

      textMessage.setText(customMessage);
      producer.send(textMessage);

    } catch (JMSException e) {
      LOG.error(e.getMessage(), e);
      e.printStackTrace();
    }
  }

  private String createCustomMessage(String msg) {
    return JsonbBuilder
        .create()
        .toJson(new CustomMessage(msg));
  }
}
